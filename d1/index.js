// Mock database
let posts = [];

// Posts ID
let count = 1;

// ADD POST DATA
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
    // Prevents the page from reloading
    // prevents default behavior of event
    e.preventDefault();

    // Adds an element in the end of an array
    posts.push({
        // "id" - used for the unique ID of each posts
        id: count,
        // "title" and "values - will come from the "form-add-post"
        title: document.querySelector("#txt-title").value,
        body: document.querySelector("#txt-body").value
    });

    // count will increment everytime a new post is added
    count++

    console.log(posts);
    alert("Post Successfully Added!");

    showPosts();

});


// RETRIEVE POSTS
const showPosts = () => {
    // "postEntries" - variable that will contain all the posts
    let postEntries = "";

    posts.forEach((post) => {
        // "+=" - adds tha value of the right operand to a variable and assigns the result to the variable
        // "div" - division on where to place the value of title and posts
        postEntries += `
            <div id ="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onClick="editPost('${post.id}')">Edit</button>
                <button onClick="deletePost('${post.id}')">Delete</button>
            </div>
        `
    });
    // To check what is stored in the properties of variables
    console.log(postEntries);
    document.querySelector("#div-post-entries").innerHTML = postEntries;

};


// EDIT POST
const editPost = (id) => {
    
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;
};


// UPDATE POST
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
    e.preventDefault();
    
    for (let i = 0; i < posts.length; i++) {

        if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
            posts[i].title = document.querySelector("#txt-edit-title").value;
            posts[i].body = document.querySelector("#txt-edit-body").value;

            showPosts(posts);
            alert("Successfully updated!");

            break;
        }
    }
});


// DELETE POST
const deletePost = (id) => {
    // Loops through all elements of the array to find the post ID that matches the post selected
    posts = posts.filter((post) => {
        if (post.id.toString() !== id) {
            return post;
        }
    });
    // The Element.remove() method removes the element from the DOM.
    document.querySelector(`#post-${id}`).remove();
};



